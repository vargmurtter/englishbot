﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Microsoft.Bot.Connector;
using Newtonsoft.Json;

namespace EnglishBot
{
    [BotAuthentication]
    public class MessagesController : ApiController
    {

        private string text, myResponse, learnPhrase;
        /// <summary>
        /// POST: api/Messages
        /// Receive a message from a user and reply to it
        /// </summary>
        public async Task<HttpResponseMessage> Post([FromBody]Activity activity)
        {
            if (activity.Type == ActivityTypes.Message)
            {

                text = activity.Text.ToLower().Trim();

                // get user data.
                StateClient stateClient = activity.GetStateClient();
                BotData userData = await stateClient.BotState.GetUserDataAsync(activity.ChannelId, activity.From.Id);

                ConnectorClient connector = new ConnectorClient(new Uri(activity.ServiceUrl));


                HttpClient httpClient = new HttpClient();
                string jsonString = await httpClient.GetStringAsync(new Uri("http://english-bot.local/"));

                dynamic words = JsonConvert.DeserializeObject(jsonString);

                int step = userData.GetProperty<int>("Step");


                if (text == "/start")
                {
                    myResponse = "Привет! Я - John, бот. Я тут, чтобы ты взялся(ась) наконец-то за английский. Переведи слово: " 
                        + words.en;

                    

                    // set steps
                    userData.SetProperty<int>("Step", 1);
                    // set word
                    userData.SetProperty<string>("CurrentJsonWord", jsonString);

                    string phrase = words.en + " = " + words.ru;
                    // set lern phrase
                    userData.SetProperty<string>("LearnPhrase", phrase);
                    // abra cadabra
                    await stateClient.BotState.SetUserDataAsync(activity.ChannelId, activity.From.Id, userData);

                }else
                {
                    if(step == 1)
                    {
                        

                        string currentJson = userData.GetProperty<string>("CurrentJsonWord");

                        dynamic currentWord = JsonConvert.DeserializeObject(currentJson);

                        string temp = currentWord.ru;

                        if(text == temp)
                        {
                            myResponse = "Правильно! Переводи дальше: " + words.en;

                            userData.SetProperty<string>("CurrentJsonWord", jsonString);

                            string phrase = words.en + " = " + words.ru;
                            // set lern phrase
                            userData.SetProperty<string>("LearnPhrase", phrase);

                            await stateClient.BotState.SetUserDataAsync(activity.ChannelId, activity.From.Id, userData);
                        }
                        else
                        {
                            myResponse = "Неа. Ты ошибся. Напиши 3 раза следующую фразу, чтобы запомнить: "
                                + userData.GetProperty<string>("LearnPhrase");

                            userData.SetProperty<int>("LearnPhraseCount", 1);
                            userData.SetProperty<int>("Step", 2);

                            await stateClient.BotState.SetUserDataAsync(activity.ChannelId, activity.From.Id, userData);

                        }

                    }else if(step == 2)
                    {

                        int count = userData.GetProperty<int>("LearnPhraseCount");
                        if(count < 3)
                        {
                            string phrase = userData.GetProperty<string>("LearnPhrase");

                            if (text == phrase)
                            {
                                myResponse = "Ага. Еще раз.";
                                count += 1;
                                userData.SetProperty<int>("LearnPhraseCount", count);

                                await stateClient.BotState.SetUserDataAsync(activity.ChannelId, activity.From.Id, userData);

                            }else
                            {
                                myResponse = "Не правильно! Пиши еще. Пока не напишешь три раза правильно, никуда не пущу.";
                            }
                        }else
                        {
                            myResponse = "Молодец! Надеюсь, это слово у тебя навсегда останется в голове. Переводи дальше: " 
                                + words.en;

                            userData.SetProperty<string>("CurrentJsonWord", jsonString);

                            string phrase = words.en + " = " + words.ru;
                            userData.SetProperty<string>("LearnPhrase", phrase);
                            userData.SetProperty<int>("LearnPhraseCount", 0);
                            userData.SetProperty<int>("Step", 1);

                            await stateClient.BotState.SetUserDataAsync(activity.ChannelId, activity.From.Id, userData);
                        }
                    }
                }

                

                Activity reply = activity.CreateReply(myResponse);
                await connector.Conversations.ReplyToActivityAsync(reply);
            }
            else
            {
                HandleSystemMessage(activity);
            }
            var response = Request.CreateResponse(HttpStatusCode.OK);
            return response;
        }
        

        private Activity HandleSystemMessage(Activity message)
        {
            if (message.Type == ActivityTypes.DeleteUserData)
            {
                // Implement user deletion here
                // If we handle user deletion, return a real message
            }
            else if (message.Type == ActivityTypes.ConversationUpdate)
            {
                // Handle conversation state changes, like members being added and removed
                // Use Activity.MembersAdded and Activity.MembersRemoved and Activity.Action for info
                // Not available in all channels
            }
            else if (message.Type == ActivityTypes.ContactRelationUpdate)
            {
                // Handle add/remove from contact lists
                // Activity.From + Activity.Action represent what happened
            }
            else if (message.Type == ActivityTypes.Typing)
            {
                // Handle knowing tha the user is typing
            }
            else if (message.Type == ActivityTypes.Ping)
            {
            }

            return null;
        }
    }
}